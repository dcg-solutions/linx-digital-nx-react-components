import styled, { keyframes, css } from 'styled-components';
import { width, height, borderRadius } from 'styled-system';

import { ParseAnimationType, ParseIsCircleType, ParseSizeType } from './types';

const getPulseAnimation = () => {
  const PulseAnimation = keyframes`
    0% {
      background-color: rgba(169, 169, 169, 0.24);
    }
    100% {
      background-color: rgba(64, 64, 64, 0.44);
    }
  `;

  return css`
    animation-name: ${PulseAnimation};
    animation-duration: 0.8s;
    animation-timing-function: linear;
    animation-iteration-count: infinite;
    animation-direction: alternate;
  `;
};

const getWaveAnimation = () => {
  const WaveAnimation = keyframes`
    from {
      background-position: -100% 0;
    }
    to {
      background-position: 100% 0;
    }
  `;

  return css`
    animation-name: ${WaveAnimation};
    animation-duration: 1s;
    animation-timing-function: reverse;
    animation-iteration-count: infinite;
    animation-direction: alternate;
    background: linear-gradient(
      90deg,
      hsl(210, 15%, 88%),
      hsl(210, 15%, 95%),
      hsl(210, 15%, 88%)
    );
    background-size: 200%;
  `;
};

const parseAnimation = (props: ParseAnimationType) => {
  if (!props.animation) {
    return '';
  }
  if (props.animation === 'pulse') {
    return getPulseAnimation();
  }
  if (props.animation === 'wave') {
    return getWaveAnimation();
  }
};

const parseIsCircle = (props: ParseIsCircleType) => {
  if (!props || !props.isCircle) {
    return '';
  }

  return css`
    border-radius: 50%;
  `;
};

const parseSize = (props: ParseSizeType) => {
  if (!props || !props.size) {
    return '';
  }

  return css`
    width: ${props.size};
    height: ${props.size};
  `;
};

export const StyledSkeleton = styled.div`
  ${parseSize};
  ${width};
  ${height};
  ${borderRadius};
  margin: 10px 0px;
  opacity: 0.7;
  ${parseAnimation};
  ${parseIsCircle};
`;
