import React from 'react';

import { SkeletonProps } from './props';
import { StyledSkeleton } from './styled';

const Skeleton = ({ dataTestid, width, height, borderRadius, animation, size, isCircle }: SkeletonProps) => {
  return (
    <StyledSkeleton
      data-testid={dataTestid}
      width={width}
      height={height}
      borderRadius={borderRadius}
      animation={animation}
      size={size}
      isCircle={isCircle}
    />
  );
};

Skeleton.defaultProps = {
  borderRadius: 'round',
  animation: 'wave'
};

export { Skeleton };