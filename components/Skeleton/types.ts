import { StyledSkeletonProps } from './props';

export type AnimationType = 'pulse' | 'wave';

export type ParseSizeType = {
  size: StyledSkeletonProps['size'];
}
export type ParseAnimationType = {
  animation: StyledSkeletonProps['animation'];
}
export type ParseIsCircleType = {
  isCircle: StyledSkeletonProps['isCircle'];
}