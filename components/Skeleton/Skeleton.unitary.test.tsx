import { render } from '@testing-library/react';
import React from 'react';

import { LayoutProvider } from '../LayoutProvider';
import { Skeleton } from './index';

jest.useRealTimers();

test('Skeleton-defaultProps-borderRadius', () => {
  const { getByTestId } = render(
    <LayoutProvider layout='default'>
      <Skeleton
        dataTestid='nx-skeleton'
        width='100px'
        height='100px'
      />
    </LayoutProvider>,
  );

  const element = getByTestId('nx-skeleton');
  const styles = getComputedStyle(element);

  expect(styles.borderRadius).toBe('var(--radius-round)');
});

test('Skeleton-defaultProps-animation', () => {
  const { getByTestId } = render(
    <LayoutProvider layout='default'>
      <Skeleton
        dataTestid='nx-skeleton'
        height='100px'
        width='100px'
      />
    </LayoutProvider>,
  );

  const element = getByTestId('nx-skeleton');
  const styles = getComputedStyle(element);

  expect(styles.animationDuration).toBe('1s');
  expect(styles.animationTimingFunction).toBe('reverse');
  expect(styles.animationIterationCount).toBe('infinite');
  expect(styles.animationDirection).toBe('alternate');

});

test('Skeleton-prop-animation-pulse', () => {
  const { getByTestId } = render(
    <LayoutProvider layout='default'>
      <Skeleton
        animation='pulse'
        dataTestid='nx-skeleton'
        isCircle
        height='100px'
        width='100px'
      />
    </LayoutProvider>,
  );

  const element = getByTestId('nx-skeleton');
  const styles = getComputedStyle(element);

  expect(styles.animationDuration).toBe('0.8s');
  expect(styles.animationTimingFunction).toBe('linear');
  expect(styles.animationIterationCount).toBe('infinite');
  expect(styles.animationDirection).toBe('alternate');
});

test('Skeleton-prop-isCircle', () => {
  const { getByTestId } = render(
    <LayoutProvider layout='default'>
      <Skeleton
        dataTestid='nx-skeleton'
        isCircle
        height='100px'
        width='100px'
      />
    </LayoutProvider>,
  );

  const element = getByTestId('nx-skeleton');
  const styles = getComputedStyle(element);

  expect(styles.borderRadius).toBe('50%');
});

test('Skeleton-prop-size', () => {
  const size = '100px';
  const { getByTestId } = render(
    <LayoutProvider layout='default'>
      <Skeleton
        dataTestid='nx-skeleton'
        size={size}
      />
    </LayoutProvider>,
  );

  const element = getByTestId('nx-skeleton');
  const styles = getComputedStyle(element);

  expect(styles.height).toBe(size);
  expect(styles.width).toBe(size);
});