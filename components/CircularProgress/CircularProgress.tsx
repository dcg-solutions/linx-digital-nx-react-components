import React from 'react';

import { CircularProgressProps } from './props';
import { parseToPx, SVG } from './styled';

const CircularProgress = ({
  dataTestid,
  color,
  backgroundColor,
  percentage,
  size,
  strokeWidth,
}: CircularProgressProps) => {
  const viewBox = `0 0 ${size} ${size}`;
  const radius = (size - strokeWidth) / 2;
  const semicircle = size / 2;
  const circumference = radius * Math.PI * 2;
  const dash = (percentage * circumference) / 100;
  const strokeDasharray = `${dash}, ${circumference - dash}`;
  const parsedStrokeWidth = parseToPx(strokeWidth);

  return (
    <SVG
      data-testid={dataTestid}
      width={size}
      semicircle={semicircle}
      height={size}
      viewBox={viewBox}
      color={color}
    >
      <circle
        id="circularProgressBackground"
        fill="none"
        stroke={backgroundColor}
        cx={semicircle}
        cy={semicircle}
        r={radius}
        strokeWidth={parsedStrokeWidth}
      />
      <circle
        id="circularProgressCircle"
        fill="none"
        cx={semicircle}
        cy={semicircle}
        r={radius}
        strokeWidth={parsedStrokeWidth}
        strokeDasharray={strokeDasharray}
        strokeLinecap="round"
      />
    </SVG>
  );
};

CircularProgress.defaultProps = {
  backgroundColor: '#f2f4f7',
  color: 'color-linx-regular',
  size: 80,
  strokeWidth: 10,
  percentage: 25,
};

export { CircularProgress };
