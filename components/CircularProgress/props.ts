import { ColorTokens } from '@linx-digital/nx-design-tokens/ts/tokens';

import { ThemeProps } from '../../theme/types';

export interface CircularProgressProps {
  dataTestid?: string;
  color?: ColorTokens;
  backgroundColor?: ColorTokens;
  percentage?: number;
  size?: number;
  strokeWidth?: number;
}

export interface StyledCircularProgressProps {
  width?: number | string;
  height?: number | string;
  semicircle?: number;
  viewBox?: string;
  color?: string;
  theme?: ThemeProps;
}
