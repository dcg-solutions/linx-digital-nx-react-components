import { ComponentType } from 'react';
import styled, { css } from 'styled-components';
import { borderRadius, margin, padding } from 'styled-system';

import { findColorValue, generateKeyboardNavigationCSS } from './parsers';
import { StyledButtonProps } from './props';

const parseDefaultColor = ({
  defaultColor,
  layout,
  textColor,
  theme,
}: StyledButtonProps) => {
  if (!defaultColor) {
    return '';
  }
  const selectedColor = findColorValue(defaultColor, theme);
  const selectedTextColor = findColorValue(textColor, theme);
  const bg =
    layout === 'outlined' || layout === 'ghost' ? 'transparent' : selectedColor;
  const border = layout === 'ghost' ? 'transparent' : selectedColor;
  return css`
    background-color: ${bg};
    border-color: ${border};
    color: ${selectedTextColor};
  `;
};
const parseActiveColor = ({
  activeColor,
  layout,
  textColor,
  theme,
}: StyledButtonProps) => {
  if (!activeColor) {
    return '';
  }
  const selectedColor = findColorValue(activeColor, theme);
  const selectedTextColor = findColorValue(activeColor, theme);
  const bg =
    layout === 'outlined' || layout === 'ghost' ? 'transparent' : selectedColor;
  const border = layout === 'ghost' ? 'transparent' : selectedColor;
  const color = layout === 'filled' ? textColor : selectedTextColor;
  const fill = layout === 'filled' ? textColor : selectedTextColor;
  return css`
    &:active {
      background-color: ${bg};
      border-color: ${border};
      color: ${color};
      svg {
        fill: ${fill};
      }
    }
  `;
};
const parseHoverColor = ({
  hoverColor,
  layout,
  textColor,
  theme,
}: StyledButtonProps) => {
  if (!hoverColor) {
    return '';
  }
  const selectedColor = findColorValue(hoverColor, theme);
  const selectedTextColor = findColorValue(hoverColor, theme);
  const bg =
    layout === 'outlined' || layout === 'ghost' ? 'transparent' : selectedColor;
  const border = layout === 'ghost' ? 'transparent' : selectedColor;
  const color = layout === 'filled' ? textColor : selectedTextColor;
  const fill = layout === 'filled' ? textColor : selectedTextColor;
  return css`
    &:hover {
      background-color: ${bg};
      border-color: ${border};
      color: ${color};
      svg {
        fill: ${fill};
      }
    }
  `;
};
const parseIconStyle = ({ textColor, theme }: StyledButtonProps) => {
  const fill =
    typeof textColor === 'string' && findColorValue(textColor, theme);
  return `
    svg {
      fill: ${fill};
      width: 16px;
      height: 16px;
    }
  `;
};
const parseKeyboardNavigation = () => {
  return generateKeyboardNavigationCSS();
};

export const StyledButton: ComponentType<StyledButtonProps> = styled.button<StyledButtonProps>`
  border-width: 1px;
  border-style: solid;
  cursor: pointer;
  height: 42px;
  display: flex;
  align-items: center;
  justify-content: center;
  ${borderRadius};
  ${margin};
  ${padding};
  ${parseDefaultColor};
  ${parseHoverColor};
  ${parseActiveColor};
  ${parseIconStyle};
  ${parseKeyboardNavigation};
`;
