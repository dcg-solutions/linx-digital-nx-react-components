export type ButtonLayoutTypes = 'filled' | 'ghost' | 'outlined';
export type ButtonSizeTypes = 'small' | 'medium' | 'large';
export type ButtonVariantTypes =
  | 'CTA'
  | 'danger'
  | 'default'
  | 'info'
  | 'line'
  | 'link'
  | 'primary'
  | 'warning';
