import { css } from 'styled-components';

export function findColorValue(color, theme) {
  if (!color || !theme) return '';
  return theme.colors[color];
}
export function generateKeyboardNavigationCSS() {
  return css`
    &:focus-visible {
      box-shadow: 0px 0px 0px 2px #ffffff,
        0px 0px 0px 4px rgba(11, 133, 239, 0.4);
      outline: unset;
    }
  `;
}
