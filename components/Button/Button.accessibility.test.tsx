import { render } from '@testing-library/react';
import { axe, toHaveNoViolations } from 'jest-axe';
import React from 'react';

import { LayoutProvider } from '../LayoutProvider';
import { Button } from './index';

jest.useRealTimers();
expect.extend(toHaveNoViolations);

describe('button-accessibility', () => {
  it('should pass without errors', async () => {
    const { container } = render(
      <LayoutProvider layout="default">
        <Button variant="primary">teste</Button>
      </LayoutProvider>,
    );

    const results = await axe(container);
    expect(results).toHaveNoViolations();
  });
});
