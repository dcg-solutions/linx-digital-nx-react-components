import { render } from '@testing-library/react';
import { toHaveNoViolations } from 'jest-axe';
import React from 'react';

import { LayoutProvider } from '../LayoutProvider';
import { Typography } from './index';

jest.useRealTimers();
expect.extend(toHaveNoViolations);

test('typography-defaultProps-fontFamily', () => {
  const { getByText } = render(
    <LayoutProvider layout='default'>
      <Typography element='h1'>
        typography-defaultProps-fontFamily
      </Typography>
    </LayoutProvider>,
  );

  const element = getByText('typography-defaultProps-fontFamily');
  const styles = getComputedStyle(element);

  expect(styles.fontFamily).toBe('var(--font-family-roboto)');
});

test('typography-prop-font', () => {
  const { getByText } = render(
    <LayoutProvider layout='default'>
      <Typography element='h1'>
        typography-prop-font
      </Typography>
    </LayoutProvider>,
  );

  const element = getByText('typography-prop-font');
  const styles = getComputedStyle(element);

  expect(styles.fontFamily).toBe('var(--font-family-roboto)');
});

test('typography-prop-dataTestid', () => {
  const { getByTestId } = render(
    <LayoutProvider layout='default'>
      <Typography dataTestid='nx-typography' element='h1'>
        The quick brown fox
      </Typography>
    </LayoutProvider>,
  );

  const element = getByTestId('nx-typography');
  const styles = getComputedStyle(element);

  expect(styles.fontFamily).toBe('var(--font-family-roboto)');
});

test('typography-prop-font-wrong', () => {
  const { getByTestId } = render(
    <LayoutProvider layout='default'>
      <Typography dataTestid='nx-typography' element='h1'>
        The quick brown fox
      </Typography>
    </LayoutProvider>,
  );

  const element = getByTestId('nx-typography');
  const styles = getComputedStyle(element);

  expect(styles.fontFamily).not.toBe('var(--font-family-arial)');
});

test('typography-prop-element', () => {
  const { getByTestId } = render(
    <LayoutProvider layout='default'>
      <Typography dataTestid='typography-prop-element' element='h1'>
        The quick brown fox
      </Typography>
    </LayoutProvider>,
  );

  const element = getByTestId('typography-prop-element');  

  expect(element.localName).toBe('h1');
});