import { render, screen } from '@testing-library/react';
import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';

import { LayoutProvider } from '../LayoutProvider';
import { Alert } from './index';

jest.useRealTimers();

test('Alert-defaultProps-borderRadius', () => {
  const { getByTestId } = render(
    <LayoutProvider layout="default">
      <Alert
        dataTestid="nx-alert"
        hasIcon
        message="Parágrafo de no máximo 3 linhas. Para não sobrecarregar o usuário com informações."
        title="Titulo em uma linha"
        variant="info"
      />
    </LayoutProvider>,
  );

  const element = getByTestId('nx-alert');
  const styles = getComputedStyle(element);
  expect(styles.borderRadius).toBe('var(--radius-round)');
});

test('Alert-Prop-FillColor', () => {
  const { getByTestId } = render(
    <LayoutProvider layout="default">
      <Alert
        dataTestid="nx-alert"
        hasIcon
        message="Parágrafo de no máximo 3 linhas. Para não sobrecarregar o usuário com informações."
        title="Titulo em uma linha"
        variant="info"
      />
    </LayoutProvider>,
  );

  const element = getByTestId('nx-alert');
  const styles = getComputedStyle(element);

  expect(styles.fill).toBe('var(--color-info-regular)');
  expect(styles.fill).not.toBe('var(--color-info-lightest)');
});

test('Alert-Prop-bgColor', () => {
  const { getByTestId } = render(
    <LayoutProvider layout="default">
      <Alert
        dataTestid="nx-alert"
        hasIcon
        message="Parágrafo de no máximo 3 linhas. Para não sobrecarregar o usuário com informações."
        title="Titulo em uma linha"
        variant="info"
      />
    </LayoutProvider>,
  );

  const element = getByTestId('nx-alert');

  expect(element).toHaveStyleRule(
    'background-color',
    'var(--color-info-lightest)',
  );
  expect(element).not.toHaveStyleRule(
    'background-color',
    'var(--color-info-regular)',
  );
});
