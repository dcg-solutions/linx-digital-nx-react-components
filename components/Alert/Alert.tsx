import {
  ActionHelpOutline,
  Close,
  InfoSolidNo,
  WarningSolidNo,
} from '@linx-digital/nx-icons/react';
import { Button, Typography } from 'nx-components';
import React from 'react';

import { StyledAlertProps } from './props';
import { StyledAlert, StyledAlertContent, StyledIconsContent } from './styled';
import { AlertVariantMapping } from './types';

const variantMapping: AlertVariantMapping = {};
variantMapping.info = {
  bg: 'color-info-lightest',
  borderColor: 'color-info-regular',
  alertIcon: <InfoSolidNo />,
};
variantMapping.warning = {
  bg: 'color-warning-lightest',
  borderColor: 'color-warning-regular',
  alertIcon: <WarningSolidNo />,
};
variantMapping.help = {
  bg: 'color-help-lightest',
  borderColor: 'color-help-regular',
  alertIcon: <ActionHelpOutline />,
};

const Alert = ({
  actionButtonClick,
  borderRadius,
  dataTestid,
  hasIcon,
  message,
  onclose,
  title,
  variant,
}: StyledAlertProps) => {
  const mappedProps = variantMapping[variant];

  return (
    <StyledAlert
      actionButtonClick={actionButtonClick}
      borderRadius={borderRadius}
      data-testid={dataTestid}
      hasIcon={hasIcon}
      message={message}
      onclose={onclose}
      variant={variant}
      {...mappedProps}
    >
      {hasIcon && (
        <StyledIconsContent>{mappedProps.alertIcon}</StyledIconsContent>
      )}

      <StyledAlertContent>
        <Typography
          element="h3"
          mt="0px"
          mb="4px"
          fontSize="font-font-size-font-size-xs"
          fontWeight="font-font-weight-font-weight-bold"
          fontFamily="roboto"
        >
          {title}
        </Typography>
        <Typography
          element="p"
          margin="0px"
          fontSize="font-font-size-font-size-xs"
          fontWeight="font-font-weight-font-weight-regular"
          fontFamily="roboto"
        >
          {message}
        </Typography>
        {actionButtonClick && (
          <Button onClick={actionButtonClick} pl="0" pr="0" variant="link">
            <Typography
              element="p"
              margin="0px"
              fontSize="font-font-size-font-size-giant"
              fontWeight="font-font-weight-font-weight-medium"
              fontFamily="roboto"
            >
              Action
            </Typography>
          </Button>
        )}
      </StyledAlertContent>
      <Close
        onClick={onclose}
        id="closeIcon"
        height="20px"
        width="20px"
        fill="#767F8D"
      />
    </StyledAlert>
  );
};

Alert.defaultProps = {
  borderRadius: 'round',
};

export { Alert };
