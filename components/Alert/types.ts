import { ColorTokens } from '@linx-digital/nx-design-tokens/ts/tokens';
import { ReactElement } from 'react';

export type AlertVariants = 'info' | 'warning' | 'help';

export type AlertVariantMappingObject = {
  alertIcon?: ReactElement;
  bg?: ColorTokens;
  borderColor?: ColorTokens;
  theme?: any;
};

export type AlertVariantMapping = {
  [key in AlertVariants]?: AlertVariantMappingObject;
};
