import {
  ColorTokens,
  RadiusTokens,
} from '@linx-digital/nx-design-tokens/ts/tokens';
import { ReactElement } from 'react';

import { AlertVariants } from './types';

export interface StyledAlertProps {
  actionButtonClick?: (any) => void;
  backgroundColor?: ColorTokens;
  border?: ColorTokens;
  borderRadius?: RadiusTokens;
  dataTestid?: string;
  iconLeft?: ReactElement;
  hasIcon?: boolean;
  message: string;
  onclose?: (any) => void;
  title?: string;
  variant: AlertVariants;
}

export interface StyledIconsContentProps {
  hasIcon?: boolean;
}
