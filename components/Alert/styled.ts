import { ComponentType } from 'react';
import styled, { css } from 'styled-components';
import { borderRadius } from 'styled-system';

import { StyledAlertProps, StyledIconsContentProps } from './props';
import { AlertVariantMappingObject } from './types';

const parseIconMargin = (hasIcon: StyledIconsContentProps) => {
  if (hasIcon) {
    return css`
      margin-right: 17px;
    `;
  }
};

const parsePaddingButton = (props) => {
  if (props.actionButtonClick) {
    return css`
      padding: 1.125rem 1.125rem 0.2rem 1.125rem;
    `;
  } else {
    return css`
      padding: 1.125rem;
    `;
  }
};

export const StyledAlertContent = styled.div`
  width: 100%;
  padding: 0rem;
`;

export const StyledIconsContent = styled.div`
  ${parseIconMargin};
  min-width: 24px;
`;

const parseAlertVariation = ({
  bg,
  borderColor,
  theme,
}: AlertVariantMappingObject) => {
  const selectedBg = theme.colors[bg];
  const selectedBorderColor = theme.colors[borderColor];
  const css = `
    background-color: ${selectedBg};
    border: 1px solid ${selectedBorderColor};
    fill: ${selectedBorderColor};
  `;
  return css;
};

export const StyledAlert: ComponentType<StyledAlertProps> = styled.div<StyledAlertProps>`
  #closeIcon {
    cursor: pointer;
  }
  ${parseAlertVariation};
  ${parsePaddingButton};
  ${borderRadius};
  width: 70%;
  display: flex;
`;
