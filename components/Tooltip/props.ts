import {
  ColorTokens,
  RadiusTokens,
} from '@linx-digital/nx-design-tokens/ts/tokens';
import { ReactNode } from 'react';
import { WidthProps } from 'styled-system';
import { ThemeProps } from 'theme/types';

import { PositionTypes } from './types';
export interface TooltipProps {
  alwaysOpen?: boolean;
  children?: ReactNode;
  dataTestid?: string;
  message?: string;
  position: PositionTypes;
  space?: string;
}

export interface StyledTooltipProps {
  alwaysOpen?: boolean;
  backgroundColor?: ColorTokens;
  borderRadius?: RadiusTokens;
  className?: string;
  'data-testid'?: string;
  position?: PositionTypes;
  space?: string;
  width?: WidthProps;
  theme?: ThemeProps;
}
