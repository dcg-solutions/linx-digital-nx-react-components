import { ComponentType } from 'react';
import styled from 'styled-components';
import { color, borderRadius, padding, width } from 'styled-system';

import { StyledTooltipProps } from './props';

//Todo: use space function from custom css-engine (this will allow support for number data type [x * factor])
function parseTipPosition({ position, space }: StyledTooltipProps) {
  const styles = {
    'bottom-center': `
      position: absolute;
      left: 50%;
      right: 50%;
      top: calc(100% + ${space});
      transform: translateX(-50%);
    `,
    'bottom-left': `
      position: absolute;
      left: 0%;
      top: calc(100% + ${space});
    `,
    'bottom-right': `
      position: absolute;
      right: 0%;
      top: calc(100% + ${space});
      `,
    'top-center': `
      position: absolute;
      left: 50%;
      right: 50%;
      bottom: calc(100% + ${space});
      transform: translateX(-50%);
      `,
    'top-left': `
      position: absolute;
      left: 0%;
      bottom: calc(100% + ${space});
      `,
    'top-right': `
      position: absolute;
      right: 0%;
      bottom: calc(100% + ${space});
      `,
  };
  return styles[position];
}

function parseDisplay({ alwaysOpen }: StyledTooltipProps) {
  if (alwaysOpen) {
    return '';
  } else {
    return 'display:none;';
  }
}

const StyledTooltip: ComponentType<StyledTooltipProps> = styled.div<StyledTooltipProps>`
  ${color};
  ${borderRadius};
  ${padding};
  ${parseDisplay}
  ${parseTipPosition};
  ${width};
  height: max-content;
`;

const StyledTooltipWrapper = styled.div`
  display: inline-block;
  position: relative;
  &:hover {
    .nx-tooltip {
      display: inline-block;
    }
  }
`;

export { StyledTooltip, StyledTooltipWrapper };
