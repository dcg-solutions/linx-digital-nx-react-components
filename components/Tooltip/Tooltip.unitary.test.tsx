import { render } from '@testing-library/react';
import React from 'react';
import 'jest-styled-components';

import { LayoutProvider } from '../LayoutProvider';
import { Tooltip } from './index';

describe('Tooltip test suite', () => {
  test('Tooltip defaultProps', () => {
    const { getByTestId } = render(
      <LayoutProvider layout="default">
        <Tooltip alwaysOpen dataTestid="nx-tooltip" position="top-left">
          nx-tooltip
        </Tooltip>
      </LayoutProvider>,
    );

    const element = getByTestId('nx-tooltip');
    const styles = getComputedStyle(element);

    //padding defaultProp
    expect(styles.padding).toBe('8px');
    //space defaultProp
    expect(styles.left).toBe('0%');
    expect(styles.bottom).toBe('calc(100% + 8px)');
  });
});
