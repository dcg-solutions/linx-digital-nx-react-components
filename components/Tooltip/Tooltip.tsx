import React from 'react';

import { Typography } from '../Typography';
import { StyledTooltipArrow } from './Arrow';
import { TooltipProps } from './props';
import { StyledTooltip, StyledTooltipWrapper } from './styled';

const Tooltip = ({
  children,
  dataTestid,
  message,
  position,
  space,
  ...otherProps
}: TooltipProps) => {
  return (
    <StyledTooltipWrapper>
      {children}
      <StyledTooltip
        backgroundColor="color-neutral-darkest"
        borderRadius="round"
        className="nx-tooltip"
        data-testid={dataTestid}
        position={position}
        space={space}
        {...otherProps}
      >
        <Typography element="span" color="color-surface">
          {message}
        </Typography>
        <StyledTooltipArrow
          backgroundColor="color-neutral-darkest"
          position={position}
          size={12}
          tooltipBorderRadius="round"
        />
      </StyledTooltip>
    </StyledTooltipWrapper>
  );
};

Tooltip.defaultProps = {
  padding: 2,
  width: 'max-content',
  space: '8px',
};

export default Tooltip;
