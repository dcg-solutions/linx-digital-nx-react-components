import {
  ColorTokens,
  RadiusTokens,
} from '@linx-digital/nx-design-tokens/ts/tokens';

import { ThemeProps } from '../../../theme/types';
import { PositionTypes } from '../types';

export interface StyledArrowProps {
  backgroundColor: ColorTokens;
  position: PositionTypes;
  size: number;
  tooltipBorderRadius: RadiusTokens;
  theme: ThemeProps;
}
