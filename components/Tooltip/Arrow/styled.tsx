import get from 'lodash/get';
import styled from 'styled-components';
import { color, size } from 'styled-system';

import { StyledArrowProps } from './props';

function parseArrowPosition(props: StyledArrowProps) {
  const { position, theme, tooltipBorderRadius } = props;
  //todo: optmize L.10 to use borderRadius function from a linx customized css-engine
  const radii = get(theme, `radii.${tooltipBorderRadius}`, '');

  const styles = {
    'top-center': `
      position: absolute;
      left: 50%;
      right: 50%;
      top: 100%;
      transform: translateX(-50%) translateY(-60%) rotate(-45deg);
    `,
    'top-left': `
      position: absolute;
      left: ${radii};
      top: 100%;
      bottom: 50%;
      transform: translateY(-60%) rotate(-45deg);
    `,
    'top-right': `
        position: absolute;
        right: calc(0% + ${radii});
        top: 100%;
        transform: translateY(-60%) rotate(-45deg);
      `,
    'bottom-center': `
        position: absolute;
        left: 50%;
        right: 50%;
        bottom: 100%;
        transform: translateX(-50%) translateY(60%) rotate(-45deg);
      `,
    'bottom-left': `
        position: absolute;
        left: calc(0% + ${radii});
        bottom: 100%;
        transform: translateY(60%) rotate(-45deg);
      `,
    'bottom-right': `
        position: absolute;
        right: calc(0% + ${radii});
        bottom: 100%;
        transform: translateY(60%) rotate(-45deg);
      `,
  };
  return styles[position];
}

const StyledTooltipArrow = styled.div<StyledArrowProps>`
  ${color};
  ${parseArrowPosition};
  ${size};
  border-radius: 2px;
`;

export { StyledTooltipArrow };
