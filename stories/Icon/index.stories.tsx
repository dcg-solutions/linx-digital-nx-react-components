import { IconQrCode } from '@linx-digital/nx-icons/react';
import React from 'react';

export default {
  title: 'Icons',
};

export const QrCode = () => {
  return (
    <React.Fragment>
      <IconQrCode width="16px" height="16px" fill="red" />{' '}
      <IconQrCode width="24px" height="24px" fill="red" />{' '}
      <IconQrCode width="40px" height="40px" fill="red" />
    </React.Fragment>
  );
};
