import { Tooltip } from 'nx-components';
import React from 'react';
import styled from 'styled-components';
import { border } from 'styled-system';

export default {
  title: 'Tooltip',
};

const Flex = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;
const Container = styled(Flex)`
  flex-direction: column;
  height: 100vh;
  width: 100%;
`;
const Tip = styled.span.attrs({
  children: 'Informação',
  border: '1px solid #000000',
})`
  ${border};
`;

export const Default = () => {
  return (
    <Container>
      <Flex>
        <Tooltip message="testando" position="top-left">
          <Tip />
        </Tooltip>
        <Tooltip message="testando" position="top-center">
          <Tip />
        </Tooltip>
        <Tooltip message="testando" position="top-right">
          <Tip />
        </Tooltip>
      </Flex>
      <Flex>
        <Tooltip message="testando" position="bottom-left">
          <Tip />
        </Tooltip>
        <Tooltip message="testando" position="bottom-center">
          <Tip />
        </Tooltip>
        <Tooltip message="testando" position="bottom-right">
          <Tip />
        </Tooltip>
      </Flex>
    </Container>
  );
};

export const AlwaysOpen = () => {
  return (
    <Container>
      <Flex>
        <Tooltip alwaysOpen message="testando" position="top-left">
          <Tip />
        </Tooltip>
        <Tooltip alwaysOpen message="testando" position="top-center">
          <Tip />
        </Tooltip>
        <Tooltip alwaysOpen message="testando" position="top-right">
          <Tip />
        </Tooltip>
      </Flex>
      <Flex>
        <Tooltip alwaysOpen message="testando" position="bottom-left">
          <Tip />
        </Tooltip>
        <Tooltip alwaysOpen message="testando" position="bottom-center">
          <Tip />
        </Tooltip>
        <Tooltip alwaysOpen message="testando" position="bottom-right">
          <Tip />
        </Tooltip>
      </Flex>
    </Container>
  );
};
