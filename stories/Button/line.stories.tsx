import { IconLove } from '@linx-digital/nx-icons/react';
import { Button } from 'nx-components';
import React from 'react';

import { SectionContainer } from './container';

export default {
  title: 'Buttons/Line',
};

export const Line = () => {
  return (
    <SectionContainer row>
      <SectionContainer>
        <Button variant="line" mb={3}>
          AdvWorks
        </Button>
        <Button variant="line" iconLeft={<IconLove />} mb={3}>
          AdvWorks
        </Button>
        <Button variant="line" iconRight={<IconLove />} mb={3}>
          AdvWorks
        </Button>
        <Button variant="line" iconLeft={<IconLove />} mb={3} />
      </SectionContainer>

      <SectionContainer ml={3}>
        <Button variant="line" mb={3} pill>
          AdvWorks
        </Button>
        <Button variant="line" iconLeft={<IconLove />} mb={3} pill>
          AdvWorks
        </Button>
        <Button variant="line" iconRight={<IconLove />} mb={3} pill>
          AdvWorks
        </Button>
        <Button variant="line" iconLeft={<IconLove />} mb={3} pill />
      </SectionContainer>
    </SectionContainer>
  );
};
