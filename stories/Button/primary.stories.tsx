import { IconLove } from '@linx-digital/nx-icons/react';
import { Button } from 'nx-components';
import React from 'react';

import { SectionContainer } from './container';

export default {
  title: 'Buttons/Primary',
};

export const Primary = () => {
  return (
    <SectionContainer row>
      <SectionContainer>
        <Button variant="primary" mb={3}>
          AdvWorks
        </Button>
        <Button variant="primary" iconLeft={<IconLove />} mb={3}>
          AdvWorks
        </Button>
        <Button variant="primary" iconRight={<IconLove />} mb={3}>
          AdvWorks
        </Button>
        <Button variant="primary" iconLeft={<IconLove />} mb={3} />
      </SectionContainer>

      <SectionContainer ml={3}>
        <Button variant="primary" mb={3} pill>
          AdvWorks
        </Button>
        <Button variant="primary" iconLeft={<IconLove />} mb={3} pill>
          AdvWorks
        </Button>
        <Button variant="primary" iconRight={<IconLove />} mb={3} pill>
          AdvWorks
        </Button>
        <Button variant="primary" iconLeft={<IconLove />} mb={3} pill />
      </SectionContainer>
    </SectionContainer>
  );
};
