import { IconLove } from '@linx-digital/nx-icons/react';
import { Button } from 'nx-components';
import React from 'react';

import { SectionContainer } from './container';

export default {
  title: 'Buttons/Link',
};

export const Link = () => {
  return (
    <SectionContainer row>
      <SectionContainer>
        <Button variant="link" mb={3}>
          AdvWorks
        </Button>
        <Button variant="link" iconLeft={<IconLove />} mb={3}>
          AdvWorks
        </Button>
        <Button variant="link" iconRight={<IconLove />} mb={3}>
          AdvWorks
        </Button>
        <Button variant="link" iconLeft={<IconLove />} mb={3} />
      </SectionContainer>

      <SectionContainer ml={3}>
        <Button variant="link" mb={3} pill>
          AdvWorks
        </Button>
        <Button variant="link" iconLeft={<IconLove />} mb={3} pill>
          AdvWorks
        </Button>
        <Button variant="link" iconRight={<IconLove />} mb={3} pill>
          AdvWorks
        </Button>
        <Button variant="link" iconLeft={<IconLove />} mb={3} pill />
      </SectionContainer>
    </SectionContainer>
  );
};
