import { IconLove } from '@linx-digital/nx-icons/react';
import { Button } from 'nx-components';
import React from 'react';

import { SectionContainer } from './container';

export default {
  title: 'Buttons/Default',
};

export const Default = () => {
  return (
    <SectionContainer row>
      <SectionContainer>
        <Button variant="default" mb={3}>
          AdvWorks
        </Button>
        <Button variant="default" iconLeft={<IconLove />} mb={3}>
          AdvWorks
        </Button>
        <Button variant="default" iconRight={<IconLove />} mb={3}>
          AdvWorks
        </Button>
        <Button variant="default" iconLeft={<IconLove />} mb={3} />
      </SectionContainer>

      <SectionContainer ml={3}>
        <Button variant="default" mb={3} pill>
          AdvWorks
        </Button>
        <Button variant="default" iconLeft={<IconLove />} mb={3} pill>
          AdvWorks
        </Button>
        <Button variant="default" iconRight={<IconLove />} mb={3} pill>
          AdvWorks
        </Button>
        <Button variant="default" iconLeft={<IconLove />} mb={3} pill />
      </SectionContainer>
    </SectionContainer>
  );
};
