import { IconLove } from '@linx-digital/nx-icons/react';
import { Button } from 'nx-components';
import React from 'react';

import { SectionContainer } from './container';

export default {
  title: 'Buttons/CTA',
};

export const CTA = () => {
  return (
    <SectionContainer row>
      <SectionContainer>
        <Button variant="CTA" mb={3}>
          AdvWorks
        </Button>
        <Button variant="CTA" iconLeft={<IconLove />} mb={3}>
          AdvWorks
        </Button>
        <Button variant="CTA" iconRight={<IconLove />} mb={3}>
          AdvWorks
        </Button>
        <Button variant="CTA" iconLeft={<IconLove />} mb={3} />
      </SectionContainer>

      <SectionContainer ml={3}>
        <Button variant="CTA" mb={3} pill>
          AdvWorks
        </Button>
        <Button variant="CTA" iconLeft={<IconLove />} mb={3} pill>
          AdvWorks
        </Button>
        <Button variant="CTA" iconRight={<IconLove />} mb={3} pill>
          AdvWorks
        </Button>
        <Button variant="CTA" iconLeft={<IconLove />} mb={3} pill />
      </SectionContainer>
    </SectionContainer>
  );
};
