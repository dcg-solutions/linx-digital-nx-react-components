import React from 'react';

import { CTA } from './cta.stories';
import { Danger } from './danger.stories';
import { Default } from './default.stories';
import { Info } from './info.stories';
import { Line } from './line.stories';
import { Link } from './link.stories';
import { Primary } from './primary.stories';
import { Warning } from './warning.stories';

export default {
  title: 'Buttons/Aggregated',
};

export const Aggregated = () => {
  return (
    <div>
      <CTA />
      <Danger />
      <Default />
      <Info />
      <Line />
      <Link />
      <Primary />
      <Warning />
    </div>
  );
};
