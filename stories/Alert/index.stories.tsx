import { Alert } from 'nx-components';
import React, { useState } from 'react';

import { SectionContainer } from '../Button/container';

export default {
  title: 'Alert',
};

export const Default = () => {
  const handleFunction = () => {
    alert('action button');
  };
  return (
    <React.Fragment>
      <SectionContainer row>
        <SectionContainer>
          <Alert
            actionButtonClick={handleFunction}
            hasIcon
            message="Parágrafo de no máximo 3 linhas. Para não sobrecarregar o usuário com informações."
            title="Titulo em uma linha"
            variant="info"
          />
          <Alert
            actionButtonClick={handleFunction}
            message="Parágrafo de no máximo 3 linhas. Para não sobrecarregar o usuário com informações."
            title="Titulo em uma linha"
            variant="info"
          />
          <Alert
            actionButtonClick={handleFunction}
            message="Parágrafo de no máximo 3 linhas. Para não sobrecarregar o usuário com informações."
            variant="info"
          />
          <Alert
            hasIcon
            message="Parágrafo de no máximo 3 linhas. Para não sobrecarregar o usuário com informações."
            title="Titulo em uma linha"
            variant="info"
          />
          <Alert
            message="Parágrafo de no máximo 3 linhas. Para não sobrecarregar o usuário com informações."
            title="Titulo em uma linha"
            variant="info"
          />
          <Alert
            hasIcon
            message="Parágrafo de no máximo 3 linhas. Para não sobrecarregar o usuário com informações."
            variant="info"
          />
        </SectionContainer>
        <SectionContainer>
          <Alert
            actionButtonClick={handleFunction}
            hasIcon
            message="Parágrafo de no máximo 3 linhas. Para não sobrecarregar o usuário com informações."
            title="Titulo em uma linha"
            variant="warning"
          />
          <Alert
            actionButtonClick={handleFunction}
            message="Parágrafo de no máximo 3 linhas. Para não sobrecarregar o usuário com informações."
            title="Titulo em uma linha"
            variant="warning"
          />
          <Alert
            actionButtonClick={handleFunction}
            message="Parágrafo de no máximo 3 linhas. Para não sobrecarregar o usuário com informações."
            variant="warning"
          />
          <Alert
            hasIcon
            message="Parágrafo de no máximo 3 linhas. Para não sobrecarregar o usuário com informações."
            title="Titulo em uma linha"
            variant="warning"
          />
          <Alert
            message="Parágrafo de no máximo 3 linhas. Para não sobrecarregar o usuário com informações."
            title="Titulo em uma linha"
            variant="warning"
          />
          <Alert
            hasIcon
            message="Parágrafo de no máximo 3 linhas. Para não sobrecarregar o usuário com informações."
            variant="warning"
          />
        </SectionContainer>
        <SectionContainer>
          <Alert
            actionButtonClick={handleFunction}
            hasIcon
            message="Parágrafo de no máximo 3 linhas. Para não sobrecarregar o usuário com informações."
            title="Titulo em uma linha"
            variant="help"
          />
          <Alert
            actionButtonClick={handleFunction}
            message="Parágrafo de no máximo 3 linhas. Para não sobrecarregar o usuário com informações."
            title="Titulo em uma linha"
            variant="help"
          />
          <Alert
            actionButtonClick={handleFunction}
            message="Parágrafo de no máximo 3 linhas. Para não sobrecarregar o usuário com informações."
            variant="help"
          />
          <Alert
            hasIcon
            message="Parágrafo de no máximo 3 linhas. Para não sobrecarregar o usuário com informações."
            title="Titulo em uma linha"
            variant="help"
          />
          <Alert
            message="Parágrafo de no máximo 3 linhas. Para não sobrecarregar o usuário com informações."
            title="Titulo em uma linha"
            variant="help"
          />
          <Alert
            hasIcon
            message="Parágrafo de no máximo 3 linhas. Para não sobrecarregar o usuário com informações."
            variant="help"
          />
        </SectionContainer>
      </SectionContainer>
    </React.Fragment>
  );
};
