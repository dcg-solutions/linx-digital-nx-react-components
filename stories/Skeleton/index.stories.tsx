import { Skeleton } from 'nx-components';
import React from 'react';

export default {
  title: 'Skeleton',
};

export const Default = () => {
  return (
    <React.Fragment>
      <Skeleton width='100px' height='100px' animation='pulse' isCircle />
      <Skeleton width='100%' height='5rem' animation='wave' />
      <Skeleton size='100px' />
      <Skeleton width='100px' height='10px' />
      <Skeleton width='200px' height='10px' />
      <Skeleton width='200px' height='10px' />
      <br />
      <Skeleton width='150px' height='10px' />
      <Skeleton width='150px' height='10px' />
      <br />
      <Skeleton width='300px' height='10px' />
      <Skeleton width='300px' height='10px' />
      <Skeleton width='300px' height='10px' />
    </React.Fragment>
  );
};
