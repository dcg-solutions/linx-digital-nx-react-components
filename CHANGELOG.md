# 1.0.0 (2021-12-21)


### Bug Fixes

* corrige entradas no arquivo .npmignore ([cb607c3](https://bitbucket.org/dcg-solutions/linx-digital-nx-react-components/commits/cb607c33011f419eeba6afa6d64cc1131252b241))
* Corrige parcialmente tipagem do Typography ([d994303](https://bitbucket.org/dcg-solutions/linx-digital-nx-react-components/commits/d994303eb8aed6271b858ab1f9665d9e9843c965))
* corrige types no StyledButton ([21b5a73](https://bitbucket.org/dcg-solutions/linx-digital-nx-react-components/commits/21b5a731908ff4f6b61b518a5c13559385dc4cda))
* eslint run ([c26a014](https://bitbucket.org/dcg-solutions/linx-digital-nx-react-components/commits/c26a0141643295aa1fd6c8e8cf7042d94be73a62))


### Features

* add CHANGELOG.md file ([a223923](https://bitbucket.org/dcg-solutions/linx-digital-nx-react-components/commits/a2239237af526f8b1681485c4e6d07731b39f6af))
* adiciona arquivo tokens.css para ser exportado no pacote final e adiciona script de cópia do css para a dist ([bf54d82](https://bitbucket.org/dcg-solutions/linx-digital-nx-react-components/commits/bf54d8253047c5983b2ea32499a0e0388c0b804a))
* Adiciona comando para cópia do .npmignore no release ([4be3ab8](https://bitbucket.org/dcg-solutions/linx-digital-nx-react-components/commits/4be3ab81c0fd26bff67ffa62ae8f4e418c35ba21))
* Adiciona passos 3 e 4 ao Readme.md ([4d35c65](https://bitbucket.org/dcg-solutions/linx-digital-nx-react-components/commits/4d35c65ec19e42b1723f2ebbda9daa108813b511))
* adiciona script do semantic release ([36aa116](https://bitbucket.org/dcg-solutions/linx-digital-nx-react-components/commits/36aa1163edca5372cba1e09995b7b5e4ddad27ff))
* Configura eslint no projeto ([3fbacd5](https://bitbucket.org/dcg-solutions/linx-digital-nx-react-components/commits/3fbacd563fe1e01426c8ac56b47a3beef70ab06b))
* configura semantic release no projeto ([289ead9](https://bitbucket.org/dcg-solutions/linx-digital-nx-react-components/commits/289ead9957a2a711c2510edd7c4011172b7b9480))
* Cria script lint:fix ([81f514f](https://bitbucket.org/dcg-solutions/linx-digital-nx-react-components/commits/81f514f6098a20bc8828626eb8fc31beb2228e7b))
* incrementa script de cópia para arquivos do npm no script prepareToPublish ([8cd18af](https://bitbucket.org/dcg-solutions/linx-digital-nx-react-components/commits/8cd18afc41725183ca87acd91d04710049ed8992))
* instala dependência cpx para cópia dos arquivos da dist ([426245d](https://bitbucket.org/dcg-solutions/linx-digital-nx-react-components/commits/426245df78329ae6d37f62f026745c58e24a6d70))
* modifica o export do theme para camel case ([c559c32](https://bitbucket.org/dcg-solutions/linx-digital-nx-react-components/commits/c559c3207661d1b8e9f7af3bba3bb244afd427b3))
