# NX Design System - React Components

[![NPM Version][npm-image]][npm-url]
[![Build Status](https://dev.azure.com/squadcheckout/Checkout/_apis/build/status/NX-DS/nx-react-components-cd?branchName=master)](https://dev.azure.com/squadcheckout/Checkout/_build/latest?definitionId=27&branchName=master)

Este repositório armazena os componentes React para o NX Design System desenvolvido pela Linx & Stone Co.

Os componentes são escritos em Typescript e utilizam a biblioteca styled-components para processamento do CSS.

A escolha do typescript preza pela excelente experiência de desenvolvimento que a linguagem proporciona, utilizando recursos como a tipagem e a funcionalidade "autosuggestion" presente no Visual Studio Code.

<br/>

> *FAQ Tecnologias que temos por aqui :D* <br/>
> node-js: https://nodejs.org/en/ <br/>
> typescript: https://www.typescriptlang.org/ <br/>
> react: https://pt-br.reactjs.org/ <br/>
> styled-components: https://styled-components.com/ <br/>
> storybook: https://storybook.js.org/

<br/>

### Quero utilizar o pacote em meu projeto
**1. Instalar softwares e dependências necessárias**

- node-js instalado no sistema (>= 16.13.1)
- typescript instalado como dependência
- react instalado como dependência
- styled-components instalado como dependência

```bash
# Instalando as dependências
npm install react styled-components typescript

# Instalando as declarações
npm install @types/node @types/react @types/react-dom @types/styled-components
```

<br/>

**2. Instalar o pacote com os componentes do NX**
```bash
# Adicionando o pacote ao meu projeto
npm install @linx-digital/nx-react-components
```

<br/>

**3. Configurando o tema**

```tsx
//Implementação em um Create React App, index.tsx

import { LayoutProvider, nxTheme } from '@linx-digital/nx-react-components'

ReactDOM.render(
  <LayoutProvider layout='default'>
    <App />
  </LayoutProvider>
  document.getElementById('root')
);
```

<br/>

**4. Configurando os tokens**

```bash
# Adicionando o pacote ao meu projeto
npm install @linx-digital/nx-design-tokens
```

Precisamos importar um arquivo CSS com as variáveis que irão prover os valores para os tokens. <br/>

O local de import será no principal arquivo javascript/typescript do seu projeto [ex: App.js ou App.tsx (CRA), main.js (Storybook)] <br/>

Também podemos associar as variáveis utilizando a tag <link> com rel="stylesheet"
```tsx
// import do arquivo variables.css realizado no path /css do pacote

import '@linx-digital/nx-design-tokens/css/variables.css'
```

Para usar os tokens basta importar um tipo do pacote instalado anteriormente.
```tsx
// import do arquivo tokens.ts realizado no path /ts do pacote

import { ColorTokens } from '@linx-digital/nx-design-tokens/ts/tokens';
```

<br/>

### Quero contribuir para a codebase

**1. Instalar as dependências**

```bash
npm install
```

**2. Rodar ambiente de desenvolvimento**

Para contribuir com o código precisamos de um ambiente de desenvolvimento que compile nosso código TSX/React.

Este repositório disponibiliza um serviço rodando um react-app com Storybook como ambiente para que seja possível renderizar os componentes e suas variações.

```bash
npm run storybook
```

**3. Ler o arquivo CONTRIBUTION.md**


[npm-image]: https://img.shields.io/npm/v/@linx-digital/nx-react-components.svg
[npm-url]: https://www.npmjs.com/package/@linx-digital/nx-react-components