import {
  ColorTokens,
  FontSizeTokens,
  FontWeightTokens,
  LineHeightTokens,
  RadiusTokens
} from '@linx-digital/nx-design-tokens/ts/tokens';

// ToDo: add FontFamilyTokens
// fonts?: { [key in FontFamilyTokens]: string };
export type ThemeProps = {
  colors?: { [key in ColorTokens]?: string };
  fonts?: object;
  fontSizes?: { [key in FontSizeTokens]: string };
  fontWeights?: { [key in FontWeightTokens]: string };
  lineHeights?: { [key in LineHeightTokens]: string };
  radii: { [key in RadiusTokens]: string };
};
